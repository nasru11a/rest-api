package ingress.az.hellospringboot.controller;

import ingress.az.hellospringboot.dto.StudentDto;
import ingress.az.hellospringboot.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @GetMapping("/{id}")
    public StudentDto getStudentByID(@PathVariable Long id){
        return service.getStudentById(id);
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>> getAllStudents(){
        return new ResponseEntity<>(service.getAllStudents(), HttpStatus.OK);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto studentDto){
        return service.createStudent(studentDto);
    }

    @PutMapping
    public StudentDto updateStudent(@RequestBody StudentDto studentDto, Long id){
        return service.updateStudent(id, studentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id){
        service.deleteStudentById(id);
    }

}
