package ingress.az.hellospringboot.repository;

import ingress.az.hellospringboot.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {}
