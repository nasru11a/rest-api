package ingress.az.hellospringboot.service;

import ingress.az.hellospringboot.dto.StudentDto;
import ingress.az.hellospringboot.model.Student;
import ingress.az.hellospringboot.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;
    private final ModelMapper mapper;

    @Override
    public StudentDto getStudentById(Long id) {
        Student student = repository.getById(id);
        return mapper.map(student, StudentDto.class);
    }

    @Override
    public List<StudentDto> getAllStudents() {
        return repository.findAll()
                .stream()
                .map(student -> mapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public StudentDto createStudent(StudentDto dto) {
        Student student = mapper.map(dto, Student.class);
        Student save = repository.save(student);
        return mapper.map(save,StudentDto.class);
    }

    @Override
    public StudentDto updateStudent(Long id, StudentDto dto) {
        Student student =
                repository.findById(dto.getId()).orElseThrow(()-> new RuntimeException("Student not found."));
        student.setName(dto.getName());
        student.setInstitute(dto.getInstitute());
        student.setBirthdate(dto.getBirthdate());
        Student save = repository.save(student);
        return mapper.map(save, StudentDto.class);
    }

    @Override
    public void deleteStudentById(Long id){
        repository.deleteById(id);
    }
}
