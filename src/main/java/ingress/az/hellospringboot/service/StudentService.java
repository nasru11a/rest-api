package ingress.az.hellospringboot.service;

import ingress.az.hellospringboot.dto.StudentDto;
import ingress.az.hellospringboot.model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    public StudentDto getStudentById(Long id);

    public List<StudentDto> getAllStudents();

    public StudentDto createStudent(StudentDto studentDto);

    public StudentDto updateStudent(Long id, StudentDto studentDto);

    public void deleteStudentById(Long id);

}
